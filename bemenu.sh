#!/bin/sh

set -e

list_path_executables() {
	printf '%s' "$1" | tr ':' '\0' | \
		xargs -0 -I{} find {} -type f -executable -printf '%f\n' | \
		sort -u
}

prog="$(list_path_executables "$PATH" | bemenu)"

sopen $prog
